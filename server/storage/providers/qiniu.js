
const qiniu = require('qiniu');

class StorageQiniu {
    constructor(config) {
        this.config = config;
        
        const qiniuConf = new qiniu.conf.Config();
        qiniuConf.regionsProvider = qiniu.httpc.Region.fromRegionId(config.region);
        qiniuConf.useHttpsDomain = false;
        this.qiniuMac = new qiniu.auth.digest.Mac(config.appId, config.appSecret);
        
        this.qiniuConf = qiniuConf;
        this.bucket = config.bucket;
    }

    async uploadFile(remotePath,localFile){
        const formUploader = new qiniu.form_up.FormUploader(this.qiniuConf);
        const putExtra = new qiniu.form_up.PutExtra();
        const putPolicy = new qiniu.rs.PutPolicy({
            scope: this.config.bucket
        });
        const uploadToken=putPolicy.uploadToken(this.qiniuMac);

        return await formUploader.putFile(uploadToken, remotePath, localFile, putExtra);
    }
}

module.exports = StorageQiniu;

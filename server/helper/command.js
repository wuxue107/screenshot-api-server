const childProcess = require("child_process");
const execFile = childProcess.execFile;
const genericPool = require("generic-pool");
const helper = require('./index');
const Config = require('../../config');
let execCommand = async function (commandFile, commandArgs, timeout) {
    let index = await commandPool.acquire();
    let subProcess;
    await new Promise(function (resolve, reject) {
        //helper.log("[" + index + "]: run command:'" + commandFile + "', with args:" + JSON.stringify(commandArgs));
        helper.log("run command:\"" + commandFile + "\"" + (commandArgs.length > 0 ? (" \"" + commandArgs.join('" "') + "\"") : ""));

        let errorMsg = '';
        subProcess = execFile(commandFile, commandArgs, {
            encoding: "buffer",
            maxBuffer: 4 * 1024 * 1024,
            timeout: timeout
        }, function (err, stdout, stderr) {
            helper.log("[" + index + "]:STDOUT:" + stdout);
            if (stderr) {
                helper.log("[" + index + "]:ERROR:" + stderr);
                errorMsg += stderr;
            }
        });

        subProcess.on('exit',function (code, signals) {
            helper.log("[" + index + "]: end command,exit code:" + code)
            commandPool.release(index);
            if(code === 0){
                resolve();
            }else{
                reject(errorMsg)
            }
        });
        
        if(!subProcess.pid){
            reject("[" + index + "]:can not run command: " + commandFile)
        }

        helper.log("[" + index + "]:PID:" + subProcess.pid);
    });
};

const createCommandPool = function (opts) {
    let cnt = 0;
    let commandFactory = {
        create: function () {
            cnt++;
            return cnt;
        },
        destroy: function (index) {
            cnt--;
        }
    };

    return genericPool.createPool(commandFactory, opts);
};

const initCommandPool = function (maxProcess) {
    if (maxProcess === undefined) {
        maxProcess = Config.maxCommand;
    }

    if (maxProcess < 2) maxProcess = 2;

    let maxCommandTaskWaiting = Config.maxCommandTaskWaiting;
    if (maxCommandTaskWaiting < 0) maxCommandTaskWaiting = 0;
    helper.info("MAX_COMMAND:" + maxProcess);
    return createCommandPool({
        max: maxProcess,
        min: 1, // minimum size of the pool
        idleTimeoutMillis: 60000,
        softIdleTimeoutMillis: 60000,
        evictionRunIntervalMillis: 1000,
        maxWaitingClients: maxCommandTaskWaiting,
    });
};

const commandPool = initCommandPool();

module.exports = {
    execCommand: execCommand,
};

(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory(require('katex')) :
  typeof define === 'function' && define.amd ? define(['katex'], factory) :
  (global = typeof globalThis !== 'undefined' ? globalThis : global || self, global.markedTocExtension = factory(global.katex));
})(this, (function (katex) { 'use strict';

  function markedKatexExtension() {
      return {
          extensions: [katexBlockExtension(), katexInlineExtension()],
      };
  }
  function katexBlockExtension() {
      const katexInputRegex = /^(\$\$)\s*\n((((?!\$\$)[^\n])*\n+)*)\1\s*(\n|$)/;
      return {
          name: 'katex-block',
          level: 'block',
          start(src) {
              var _a;
              return (_a = src.match(/^\$\$/)) === null || _a === void 0 ? void 0 : _a.index;
          },
          tokenizer(src) {
              const match = katexInputRegex.exec(src);
              if (match) {
                  return {
                      type: 'katex-block',
                      raw: match[0],
                      text: match[2].trim(),
                      tokens: [],
                  };
              }
          },
          renderer(token) {
              return katex.renderToString(token.text, {
                  output: 'html',
                  throwOnError: false,
              });
          },
      };
  }
  function katexInlineExtension() {
      const katexInlineStartRegex = /\$([^$]+)\$/;
      const katexInlineRegex = new RegExp(`^${katexInlineStartRegex.source}`);
      return {
          name: 'katex-inline',
          level: 'inline',
          start(src) {
              var _a;
              return (_a = src.match(katexInlineStartRegex)) === null || _a === void 0 ? void 0 : _a.index;
          },
          tokenizer(src) {
              const match = katexInlineRegex.exec(src);
              if (match) {
                  return {
                      type: 'katex-inline',
                      raw: match[0],
                      katex: match[1],
                  };
              }
          },
          renderer(token) {
              return katex.renderToString(token.katex, {
                  output: 'html',
                  throwOnError: false,
              });
          },
          childTokens: ['katex'],
      };
  }

  return markedKatexExtension;

}));

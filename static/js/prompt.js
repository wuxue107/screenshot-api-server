(function (){
    let callback = {}
    window.__monkeyOnDialog = function (type,cb){
        if( callback[type] === undefined) callback[type] = [];
        callback[type].push(cb);
    }
    
    const originalPrompt = window.prompt
    const originalConfirmation = window.confirm
    const originalAlert = window.alert

    const getFrameLocation = function () {
        let frameLocation = ''
        let currentWindow = window
        let currentParentWindow
        while (currentWindow !== window.top) {
            currentParentWindow = currentWindow.parent
            for (let idx = 0; idx < currentParentWindow.frames.length; idx++)
                if (currentParentWindow.frames[idx] === currentWindow) {
                    frameLocation = ':' + idx + frameLocation
                    currentWindow = currentParentWindow
                    break
                }
        }
        frameLocation = 'root' + frameLocation
        return frameLocation
    }

    let promptIndex = 0;
    window.prompt = function(text, value = '') {
        console.log("prompt index:"+promptIndex+", text:" + text + ",value:" + value)
        let cbs = callback['prompt'] ?? [];
        let location = getFrameLocation()
        let ret = null;
        for (const cb of cbs) {
            let val = cb({
                index: promptIndex,
                location,
                text,
                value,
            });
            if(val !== undefined){
                if(val !== null && typeof val !== 'string'){
                    console.error("prompt index:"+promptIndex+", return value must be a string or null")
                }
                ret = val;
                break;
            }
        }
        promptIndex++;
        console.log("prompt index:"+promptIndex+",return: "+ ret);
        return ret;
    }
    window.prompt.toString = function (){
        return originalPrompt.toString();
    }
    
    let confirmIndex = 0;
    window.confirm = function(text) {
        console.log("confirm index:"+confirmIndex+", text:" + text)
        let cbs = callback['confirm'] ?? [];
        let location = getFrameLocation()
        let ret = false;
        for (const cb of cbs) {
            let val = cb({
                index: confirmIndex,
                location,
                text
            });
            if(val !== undefined){
                if(typeof val !== "boolean"){
                    console.warn("confirm return must be a boolean")
                }
                ret = !!val;
                break;
            }
        }
        
        console.log("confirm index:"+promptIndex+",return: "+ (ret?'true':'false'));

        promptIndex++;
        return ret;
    }
    window.confirm.toString = function (){
        return originalConfirmation.toString();
    }
    
    let alertIndex = 0;
    window.alert = function(text) {
        console.log("alert index:"+alertIndex+", text:" + text)
        let cbs = callback['alert'] ?? [];
        let location = getFrameLocation()
        for (const cb of cbs) {
            cb({
                index: confirmIndex,
                location,
                text
            });
        }
        
        promptIndex++;
    }
    window.alert.toString = function (){
        return originalAlert.toString();
    }
})();

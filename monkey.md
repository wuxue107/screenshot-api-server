# 油猴交互


## Monkey.end() 结束油猴，API返回数据


## Monkey.data(key, data) 上报采集数据
- key ,上报数据，对应的字段名
- data, 上报数据

## Monkey.screenshot(key, selector,type = 'png') 单张截图
- key ,上报数据，对应的字段名
- selector, 节点选择器如： ".btn"、"body>table"
- type 图片格式 png | jpeg

## Monkey.screenshots(key, selector,type = 'png') 多张截图
- key ,上报数据，对应的字段名
- selector, 节点选择器如： ".btn"、"body>table"
- type 图片格式 png | jpeg

## Monkey.getCookies() 获取cookie（防止在httponly模式下，page js无法获取js的情况）


## Monkey.onDialog(dialogType,callback) 原生弹窗拦截处理
- 使用js自定义的函数，覆盖了原生的弹窗函数，
- dialogType： 弹窗类型： alert、prompt、confirm 
- callback: 回调函数
```javascript

Monkey.onDialog('alert',callback);
// 当有触发 alert("abc") 时
callback({ 
  index: 0,
  text : "abc",
});

Monkey.onDialog('confirm',callback)
// 当有触发 confirm("abc") 时
callback({ 
  index: 0,
  text : "abc",
})
// callback应当返回 boolean,告知是否确认


Monkey.onDialog('prompt',callback)
// 当有触发 confirm("请填写标题","默认值") 时
callback({
    index: 0,
    text : "请填写标题",
    value : "默认值",
})
// callback应当返回 string | null 告知prompt返回结果
```

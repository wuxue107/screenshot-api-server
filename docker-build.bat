@echo off

set SCRIPT_PATH=%~dp0
cd %SCRIPT_PATH%

node --eval="console.log(require('./package').version);" >version.tmp
set /p IMAGE_VERSION=<version.tmp
del /f version.tmp

set /p FAST_BASE_VERSION=<.\docker\FAST_BASE_VERSION
set /p BASE_VERSION=<.\docker\BASE_VERSION

set IMAGE_TYPE=%1
set IMAGE_ARCH=%2


if "%IMAGE_TYPE%" == "" (
    set IMAGE_TYPE=fast
)

if "%IMAGE_ARCH%" == "" (
    set IMAGE_ARCH=amd64
)

if "%IMAGE_ARCH%" == "amd64" (
    set BASE_IMAGE_TAG=%BASE_VERSION%
    set FAST_BASE_IMAGE_TAG=%FAST_BASE_VERSION%
    set IMAGE_TAG=%IMAGE_VERSION%
) else (
    set BASE_IMAGE_TAG=%BASE_VERSION%-%IMAGE_ARCH%
    set FAST_BASE_IMAGE_TAG=%FAST_BASE_VERSION%-%IMAGE_ARCH%
    set IMAGE_TAG=%IMAGE_VERSION%-%IMAGE_ARCH%
)

set DOCKER_FILE_PATH=.\docker\%IMAGE_ARCH%\

:: 包含nodejs、wkhtmlpdf、chrome所需依赖、jdk
if "%%IMAGE_TYPE%%" == "base" (
    docker rmi wuxue107/screenshot-api-base:%BASE_IMAGE_TAG%
    echo docker build -f %DOCKER_FILE_PATH%Dockerfile-Base -t wuxue107/screenshot-api-base:%BASE_IMAGE_TAG% .
    docker build -f %DOCKER_FILE_PATH%Dockerfile-Base -t wuxue107/screenshot-api-base:%BASE_IMAGE_TAG% .
)

:: 包含在base镜像的基础上添加: screenshot-api-server的nodejs依赖和puppeteer chrome
if "%IMAGE_TYPE%" == "fastbase" (
    echo docker rmi wuxue107/screenshot-api-server-fast-base:%FAST_BASE_IMAGE_TAG%
    docker rmi wuxue107/screenshot-api-server-fast-base:%FAST_BASE_IMAGE_TAG%
    echo docker build --build-arg BASE_IMAGE_TAG=%BASE_IMAGE_TAG% -f %DOCKER_FILE_PATH%Dockerfile-FastBase -t wuxue107/screenshot-api-server-fast-base:%FAST_BASE_IMAGE_TAG% .
    docker build --build-arg BASE_IMAGE_TAG=%BASE_IMAGE_TAG% -f %DOCKER_FILE_PATH%Dockerfile-FastBase -t wuxue107/screenshot-api-server-fast-base:%FAST_BASE_IMAGE_TAG% .
)

:: 在fastbase镜像基础上，将当前screenshot-api-server程序包拷贝进去
if "%IMAGE_TYPE%" == "fast" (
    git archive --format=tar --worktree-attributes --prefix=screenshot-api-server/ -o latest.tar HEAD
    
    docker rmi wuxue107/screenshot-api-server:%IMAGE_TAG%
    echo docker build --build-arg FAST_BASE_IMAGE_TAG=%FAST_BASE_IMAGE_TAG% -f %DOCKER_FILE_PATH%Dockerfile-Fast -t wuxue107/screenshot-api-server:%IMAGE_TAG% .
    docker build --build-arg FAST_BASE_IMAGE_TAG=%FAST_BASE_IMAGE_TAG% -f %DOCKER_FILE_PATH%Dockerfile-Fast -t wuxue107/screenshot-api-server:%IMAGE_TAG% .
    del /f latest.tar
    
    if "%IMAGE_ARCH%" == "amd64" (
        docker rmi wuxue107/screenshot-api-server:latest
        docker tag wuxue107/screenshot-api-server:%IMAGE_TAG% wuxue107/screenshot-api-server:latest
    )
)

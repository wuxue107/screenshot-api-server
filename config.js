const os = require("os");
const getDefaultMaxBrowserCount = function (){
    let freeMem = os.freemem() / (1024 * 1024);
    // 单浏览器实例，配备2cpu和250M内存
    return Math.max(1,~~Math.min(os.cpus().length * 0.7,freeMem / 250))
}

const getDefaultMaxCommandCount = function (){
    return Math.max(2,os.cpus().length * 1.5);
}

let maxBrowser = ~~ process.env.MAX_BROWSER || getDefaultMaxBrowserCount();
let maxCommand = ~~ process.env.MAX_COMMAND || getDefaultMaxCommandCount();

const Config = {
    appDebug: process.env.APP_DEBUG === 'true',
    serverPort: process.env.SERVER_PORT || '3000',

    // 本地储存，PDF保存1天 , 0: 不清理PDF文件
    pdfKeepDay : ~~process.env.PDF_KEEP_DAY,
    
    // 最大浏览器实例数,0: 未指定则
    maxBrowser: maxBrowser,
    // 浏览器任务排队最大任务数, 参考下平均任务时长，防止超时。 
    // 超过最大排队数直接响应失败，如果平均任务时长较短，可以设置一个较大的倍率
    maxBrowserTaskWaiting: maxBrowser * 10,
    // 物理像素和逻辑像素比例，清晰度
    browserDeviceScaleFactor: process.env.DEVICE_SCALE_FACTOR || '2.0',
    // 最大命令行进程数（wkhtmltopdf）
    maxCommand: maxCommand,
    // 命令行任务排队最大任务数, 参考下平均任务时长，防止超时。 
    // 超过最大排队数直接响应失败，如果平均任务时长较短，可以设置一个较大的倍率
    maxCommandTaskWaiting: maxBrowser * 15,
    
    // 阻止host为ip的URL资源代理
    blockIpUrl : !!process.env.BLOCK_IP_URL,
    
    // 浏览器程序执行程序路径
    puppeteerExecutablePath : process.env.PUPPETEER_EXECUTABLE_PATH,

    // 制作的pdf文件储存配置
    storage : {
        type: process.env.STORAGE_TYPE || 'file',
        bucket: process.env.STORAGE_BUCKET,
        region: process.env.STORAGE_REGION,
        appId : process.env.STORAGE_APP_ID,
        appSecret : process.env.STORAGE_APP_SECRET,
    }
}




module.exports = Config;

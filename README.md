# 网页截图 和 生成PDF Api服务

    使用node express和puppeteer搭建的WEB截图服务

## 使用docker方式 安装
- docker仓库为：<a href="https://gitee.com/wuxue107/screenshot-api-server" target="_blank">wuxue107/screenshot-api-server</a>
- 容器内目录web根目录 /screenshot-api-server/public 为可挂载目录，里面可以放一些静态文件
- 使用运行下面命令，会将当前目录作为，web根目录运行web服务，

```bash
docker pull wuxue107/screenshot-api-server

## -e MAX_BROWSER=[num] 环境变量可选，最大的puppeteer实例数，忽略选项则默认值:1 , 值auto：[可用内存]/200M
##  -e PDF_KEEP_DAY=[num] 自动删除num天之前产生的文件目录,默认0: 不删除文件
docker run -p 3000:3000 -td --rm -e MAX_BROWSER=1 -e PDF_KEEP_DAY=0 -v ${PWD}:/screenshot-api-server/public --name=screenshot-api-server wuxue107/screenshot-api-server
```

## 环境变量

- 详情参考：[环境变量](environments.md)

## 本地使用

```bash
yarn && yarn start
```

# API 接口

## 截图

### 注意事项：

    生成的dataUrl图片，如果过大，粘贴到浏览器地址栏上显示的图片有时会显示不完整。
    并不是只截取了一半，而是因为浏览器URL浏长度限制的原因。
    将其复制给图片src属性，或保存为文件，则可以看到完整图片

### 单张图片截取

- API: http://localhost:3000/api/img
- 请求参数：PSOT JSON

```javascript
{
    // 要截图的网页 (pageUrl 、html 参数二选一）
    "pageUrl":"https://gitee.com/wuxue107",
    // 要截图的网页HTML (pageUrl 、html 参数二选一）
    "html" : "",
    // 要截取的节点选择器,可选，默认body
    "element":"body",
    // 超时时间，可选，默认：30000
    "timeout": 30000,
    // 检查页面是否渲染完成的js表达式，可选，默认: "true"
    "checkPageCompleteJs": "document.readyState === 'complete'",
    // 页面完成后（checkPageCompleteJs返回为true后）延迟的时间，可选，默认：0
    "delay": 100,
    // 浏览器视窗宽度、高度。在响应式布局中，有时会体现出不同样式
    "width": 1920,
    "height": 1280
}
```

- 响应

```javascript
{
  "code": 0,
  "msg": "success",
  "data": {
    "image": "data:image/png;base64,..."
  }
}
```

### 多张图片截取

- 请求参数：PSOT JSON
- API: http://localhost:3000/api/imgs

```javascript
{
    // 要截图的网页 (pageUrl 、html 参数二选一）
    "pageUrl": "https://gitee.com/wuxue107",
    // 要截图的网页HTML (pageUrl 、html 参数二选一）
    "html": "",
    // 要截取的节点选择器,可选，默认body
    "elements": [".card"],
    // 超时时间，可选，默认：30000
    "timeout": 30000,
    // 检查页面是否渲染完成的js表达式，可选，默认: "true"
    "checkPageCompleteJs": "document.readyState === 'complete'",
    // 页面完成后（checkPageCompleteJs返回为true后）延迟的时间，可选，默认：0
    "delay": 100,
    // 浏览器视窗宽度、高度。在响应式布局中，有时会体现出不同样式
    "width": 1920,
    "height": 1280
}
```

- 响应

```json
{
    "code": 0,
    "msg": "success",
    "data": {
        "images": {
            ".card": [
                "data:image/png;base64,..."
            ]
        }
    }
}
```

## bookjs-easy PDF生成

- 生成由 <a href="https://gitee.com/wuxue107/bookjs-eazy" target="_blank">wuxue107/bookjs-eazy</a> 制作的PDF页面

### 根据 bookjs-eazy 模板片段生成PDF (Chrome Headless)

- API: http://localhost:3000/api/book-tpl
- 请求参数：PSOT JSON，请设置一个较长的超时时间
- 测试页面： [在线测试](https://bookjs.zhouwuxue.com/static/book-tpl/editor.html)

```javascript
{
    // PDF 配置：参考 <a href="https://gitee.com/wuxue107/bookjs-eazy#%E9%85%8D%E7%BD%AE%E9%A1%B5%E9%9D%A2%E5%8F%82%E6%95%B0" target="_blank">wuxue107/bookjs-eazy 配置页面参数</a> 
    "bookConfig": {
         "pageSize": "ISO_A4",
         "orientation": "portrait",
         "padding": "20mm 10mm 20mm 10mm"
    },
    // 额外的css样式
    "bookStyle" : `a{color:red;}`,
    // 模板内容：参考 <a href="https://gitee.com/wuxue107/bookjs-eazy#pdf%E5%86%85%E5%AE%B9%E8%AE%BE%E8%AE%A1" target="_blank">wuxue107/bookjs-eazy 内容设计. #contentBox内部的HTML</a> 
    "bookTpl" : `<div data-op-type="new-page"></div><div data-op-type="pendants"><div class='pendant-title'>第一章：Echart图表</div></div><h1  data-op-type='block'>第1章 Echart图表</h1>`,
    // 超时时间，可选，默认：30000
    "timeout": 30000,
    "delay": 100
}
```

- 响应，生成的pdf文件存放在web可挂载的web目录下,路径/pdf/xxxx.pdf

```javascript
{
  "code": 0,
  "msg": "success",
  "data": {
    // 拼接上接口的前缀 http://localhost:3000/ 就是完整PDF地址 
    // http://localhost:3000/pdf/1614458263411-glduu.pdf
    // 拼接上接口的前缀 http://localhost:3000/download/可以就可生成在浏览器上的下载链接
    // http://localhost:3000/download/pdf/1614458263411-glduu.pdf
    // 拼接上http://localhost:3000/static/js/pdfjs/web/viewer.html?file=/pdf/1614458263411-glduu.pdf
    // 可使用pdfjs库进行预览
    "file": "/pdf/1614458263411-glduu.pdf"
  }
}
```

### 根据bookjs-eazy 网页生成PDF(Chrome Headless)

- API: http://localhost:3000/api/book
- 请求参数：PSOT JSON，请设置一个较长的超时时间

```javascript
{
    // 由bookjs-eazy制作的网页
    "pageUrl": "https://bookjs.zhouwuxue.com/eazy-2.html",
    // 超时时间，可选，默认：30000
    "timeout": 30000,
    // 页面完成后（checkPageCompleteJs返回为true后）延迟的时间，可选，默认：0
    "delay": 100
}
```

- 响应，生成的pdf文件存放在web可挂载的web目录下,路径/pdf/xxxx.pdf

```javascript
{
  "code": 0,
  "msg": "success",
  "data": {
    // 拼接上接口的前缀 http://localhost:3000/ 就是完整PDF地址 
    // http://localhost:3000/pdf/1614458263411-glduu.pdf
    // 拼接上接口的前缀 http://localhost:3000/download/可以就可生成在浏览器上的下载链接
    // http://localhost:3000/download/pdf/1614458263411-glduu.pdf
    // 拼接上http://localhost:3000/static/js/pdfjs/web/viewer.html?file=/pdf/1614458263411-glduu.pdf
    // 可使用pdfjs库进行预览
    "file": "/pdf/1614458263411-glduu.pdf"
  }
}
```

### 根据bookjs-eazy 网页生成PDF(wkhtmltopdf)

- API: http://localhost:3000/api/wkhtmltopdf-book
- 请求参数：PSOT JSON，请设置一个较长的超时时间

```javascript
{
    // 由bookjs-eazy制作的网页
    "pageUrl": "https://bookjs.zhouwuxue.com/eazy-2.html",
    // 超时时间，可选，默认：30000
    "timeout": 30000,
    // 页面完成后（checkPageCompleteJs返回为true后）延迟的时间，可选，默认：0
    "delay" : 100,
    // 纸张
    "orientation": "portrait", // 纸张方向 "portrait"，"landscape"
    "pageSize":"A4", // 纸张大小
    // 或 
    "pageWidth" : "210", //mm
    "pageHeight" : "297",

}
```

- 响应，生成的pdf文件存放在web可挂载的web目录下,路径/pdf/xxxx.pdf

```javascript
{
  "code": 0,
  "msg": "success",
  "data": {
    // 拼接上接口的前缀 http://localhost:3000/ 就是完整PDF地址 
    // http://localhost:3000/pdf/1614458263411-glduu.pdf
    // 拼接上接口的前缀 http://localhost:3000/download/可以就可生成在浏览器上的下载链接
    // http://localhost:3000/download/pdf/1614458263411-glduu.pdf
    // 拼接上http://localhost:3000/static/js/pdfjs/web/viewer.html?file=/pdf/1614458263411-glduu.pdf
    // 可使用pdfjs库进行预览
    "file": "/pdf/1614458263411-glduu.pdf"
  }
}
```

## 通用网页生成PDF

- API: http://localhost:3000/api/pdf
- 请求参数：PSOT JSON，请设置一个较长的超时时间

```javascript
{
    // 要制作为PDF的网页 (pageUrl 、html 参数二选一）
    "pageUrl":"https://gitee.com/wuxue107",
    // 要截图的网页HTML (pageUrl 、html 参数二选一）
    "html" : "<div>bookjs-eazy</div>",
    // 超时时间，可选，默认：30000
    "timeout": 30000,
    // 检查页面是否渲染完成的js表达式，可选，默认: "true"
    "checkPageCompleteJs": "true",
    // 页面完成后（checkPageCompleteJs返回为true后）延迟的时间，可选，默认：0
    "delay": 100
}
```

- 响应，生成的pdf文件存放在web可挂载的web目录下,路径/pdf/xxxx.pdf

```javascript
{
  "code": 0,
  "msg": "success",
  "data": {
    // 拼接上接口的前缀 http://localhost:3000/ 就是完整PDF地址 
    // http://localhost:3000/pdf/1614458263411-glduu.pdf
    // 拼接上接口的前缀 http://localhost:3000/download/可以就可生成在浏览器上的下载链接
    // http://localhost:3000/download/pdf/1614458263411-glduu.pdf
    // 拼接上http://localhost:3000/static/js/pdfjs/web/viewer.html?file=/pdf/1614458263411-glduu.pdf
    // 可使用pdfjs库进行预览
    // 如果配置了云存储： 则可以直接拼接上的的云存储源域名或CDN域名
    "file": "/pdf/1614458263411-glduu.pdf"
  }
}
```

<!--

## 自定义爬虫，浏览器的任意操作
### 自定义油猴

- 通过向目标页面注入传入的Js脚本,Js负责自动化操作，和抽取数据，通过向页面注入原生js交互实现内容抽取
- 页面默认注入了jQuery，可通过jQuery进行DOM操作
- 
- 具体实现参考，与原生交互，参考 [油猴交互](monkey.md)
- 
- 请求参数：PSOT JSON
- API: http://localhost:3000/api/monkey

```javascript
{
    "pageUrl": "http://xxxx.com/目标站点",
    // 在document加载前就会执行的js脚本，如可以在这里进行原生弹窗拦截
    "preload": `
        // 所有的confirm，选择<确认>
        Monkey.onDialog("confirm",function(data){
            return true;
        })
    `,
    "monkey": `
        //"用于实现自动化逻辑Js"
        $('.btn').click();
        // 上报数据到接口
        await Monkey.data('title',document.title);
        await Monkey.data('listCount',$('.item').length);
        // 结束,调用此方法告诉通知NodeJs，油猴操作结束
        Monkey.end();
    `,
    // 超时时间，可选，默认：30000
    "timeout": 30000,
    "width": 1920,
    "height": 1280
}
```

- 响应

```json
{
    "code": 0,
    "msg": "success",
    "data": {
        "title": "xxxxx",
        "listCount": 100,
    }
}
```


### 自定义蜘蛛

- 这部分需要，自行实现Node爬虫方法，在public/spider/index.js（在docker挂载目录public中,以便使用，文件不存在创建即可）
- 或server/spider/index.js中实现爬虫函数并导出
- 导出的函数名，对应请求参数中的spiderName
- 请求参数：PSOT JSON
- API: http://localhost:3000/api/spider

```javascript
{
    "spiderName": "demoBaiduSearch",
    // 超时时间，可选，默认：30000
    "timeout": 30000,
    "width": 1920,
    "height": 1280
}
```

- 响应

```json
{
    "code": 0,
    "msg": "success",
    "data": {
        // 内容由具体爬虫自定义
    }
}
```

-->

# 内置静态资源

- http://localhost:3000/static/ 下内置了 <a href="https://gitee.com/wuxue107/bookjs-eazy" target="_blank">bookjs-eazy</a>的一些依赖静态资源

```
static/js
    - bookjs/
        latest/
            bookjs-eazy.min.js
    - pdfjs/
        web/viewer.html?file=/pdf/2021-03-24/xxxx.pdf 
    - jquery.min.js
    - lodash.min.js
    - polyfill.min.js

```

# 字体安装使用

- 放入web根目录./fonts下的所有字体文件，会在docker启动时自动加载，如果是本机部署则手动执行下或参考：install-font.sh
- 为了加快截图或生成PDF速度,通常字体文件较大，下载耗时。防止渲染截图或PDF出现字体不一致情况，建议预先安装常用字体
- 如果是自己设计的页面，建议css设置字体时，优先使用字体原字体名，再使用网络字体别名，例如：

```html

<style>
    @font-face {
        font-family: YH;
        src: url(./fonts/msyh.ttf);
        font-weight: 400;
        font-style: normal
    }

    body {
        font-family: "Microsoft YaHei", YH, sans-serif;
        font-weight: normal;
    }
</style>
```

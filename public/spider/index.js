const {screenshotDOMElement,screenshotDOMElements,renderPdf} = require('../../server/puppeteer/util')
const helper = require("../../server/helper");

/**
 * 百度Spider示例
 * 
 * @param {Page} page
 * @param {Object} params
 * 
 * @returns {Promise<{result: *, pics: Object}>} 接口传入的post json的参数
 */
const demoBaiduSearch = async function(page,params){
    // 跳转到百度首页
    await page.goto('https://www.baidu.com');
    // 找到搜索框并输入搜索关键词
    await page.type('#kw', params.keyword, { delay: 100 }); // 模拟用户输入，每个字符之间有100ms的延迟
    // 模拟按下回车键进行搜索
    await page.keyboard.press('Enter');
    // 等待搜索结果页面加载完成
    // 这里使用等待选择器出现作为加载完成的标志
    await page.waitForSelector('.result', { timeout: 5000 }); // 等待5秒，直到.result类名的元素出现
    await helper.wait(1000);

    // 抽取搜索数据
    const result = await page.$$eval('.result',async function (els){
        return els.map(function (el){
            return {
                title: el.querySelector('.c-title').textContent,
                content: el.textContent,
            }
        });
    })
    // 搜索数据截图
    const pics =  await screenshotDOMElements(page,['.result']);
    
    // 接口/api/spider 返回数据
    return {
        result,
        pics
    }
}

module.exports = {
    demoBaiduSearch,
}

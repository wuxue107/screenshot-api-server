## 环境变量配置

| 环境变量               | 参数说明                                             | 默认值                                                          |
|:-------------------|:-------------------------------------------------|:-------------------------------------------------------------|
| APP_DEBUG          | 调试信息，打印更详细的日志                                    | 默认：false / true                                              |
| SERVER_PORT        | 服务端口                                             | 3000                                                         |
| PDF_KEEP_DAY       | 本地储存，PDF保留天数，超过指定天数会自动清理                         | 0: 不清理PDF文件                                                  |
| MAX_BROWSER        | 浏览器实例池，最大实例数,最少1个                                | 不指定则自动计算                                                     |
| MAX_COMMAND        | 命令行程序实例池(wkhtmltopdf)，最大实例数实例数(wkhtmltopdf)，最少2个 | 不指定则自动计算                                                     |
| STORAGE_TYPE       | PDF存储介质                                          | file: 默认本地文件， qiniu:七牛云存储、ali:阿里OSS、tencent:腾讯COS、upyun: 又拍云 |
| STORAGE_BUCKET     | 储存桶ID、应用（又拍云）                                    |                                                              |
| STORAGE_REGION     | 地区ID                                             |                                                              |
| STORAGE_APP_ID     | APPID、操作员（又拍云）                                   |                                                              |
| STORAGE_APP_SECRET | APP密钥、密码（又拍云）                                    |                                                              |

```
        // 阿里OSS
        STORAGE_TYPE: "ali",
        STORAGE_BUCKET: "[bucket-name]",
        STORAGE_REGION: "oss-cn-qingdao",
        STORAGE_APP_ID : "xxxx",
        STORAGE_APP_SECRET : "xxxxx",
            
        // 腾讯COS
        STORAGE_TYPE: "tencent",
        STORAGE_BUCKET: "[bucket-name]",
        STORAGE_REGION: "ap-shanghai",
        STORAGE_APP_ID : "xxxx",
        STORAGE_APP_SECRET : "xxxx",
        
        // 七牛云
        STORAGE_TYPE: 'qiniu',
        STORAGE_BUCKET: '[bucket-name]',
        // 华东-浙江	z0
        // 华东-浙江2	cn-east-2
        // 华北-河北	z1
        // 华南-广东	z2
        // 北美-洛杉矶	na0
        // 亚太-新加坡（原东南亚）	as0
        // 亚太-河内	ap-southeast-2
        // 亚太-胡志明	ap-southeast-3
        STORAGE_REGION: 'z0'
        STORAGE_APP_ID : 'xxxx',
        STORAGE_APP_SECRET : 'xxxxx',
        
        // 又拍云
        STORAGE_TYPE: “upyun”,
        STORAGE_BUCKET: "[应用ID]", // 应用
        STORAGE_APP_ID : "[操作员]", // 操作员
        STORAGE_APP_SECRET : "[操作员密码]", // 密码
```
